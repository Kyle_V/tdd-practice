public class User {
    
    public String username;
    public String password;
    public UserType userType;


    public User(String username, String password, UserType userType) {
        this.username = username;
        this.password = password;
        this.userType = userType;
    }


    public UserType getType() { return this.userType; }
    public String getUsername() { return this.username; }
    public String getPassword() { return this.password; }

    
}
